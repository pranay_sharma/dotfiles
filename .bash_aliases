#git related
alias  gitl='git log --graph --decorate --pretty=oneline --abbrev-commit'
alias  gits='git status'
    #set the default editor for GIT to nano. Refrence: http://stackoverflow.com/questions/2596805/how-do-i-make-git-use-the-editor-of-my-choice-for-commits
    #Todo: This should be in .bashrc
    git config --global core.editor "nano"
    export GIT_EDITOR=nano



#bash related
# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll="ls -alrtF -color"
alias la='ls -A'
alias l='ls -CF'
alias wh='watch -color -n 1'
# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\
\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

alias rm="rm -i"
alias mv="mv -i"
alias cp="cp -i"
set -o noclobber

# 2.2) Listing, directories, and motion
alias dir='ls --color=auto --format=vertical'
alias vdir='ls --color=auto --format=long'
alias m='less'
alias ..='cd ..'
alias ...='cd ..;cd ..'
alias md='mkdir'
alias cl='clear'
alias du='du -ch --max-depth=1'
alias treeacl='tree -A -C -L 2'

#Tail related
alias tail='tail -F -n 100'

# Cool stuff
alias jsonget="curl -X GET -H 'Accept: application/json'"

#django related
alias pms='pypy manage.py syncdb'
alias pmmi='pypy manage.py makemigrations'
alias pmdm='pypy manage.py deletemigrations'
alias pmm='pypy manage.py migrate'
alias pmsh='pypy manage.py shell'
#create superuser
alias pmc='pypy manage.py createsupertmquser'
#clear database
alias pmf='pypy manage.py flush'
alias pm='pypy manage.py'
alias pmd='pypy manage.py deletemigrations'
alias pmp='pypy manage.py populate'
alias pmcs='pypy manage.py createstatic --noinput'

#supervisord commands for logic_Srv
alias logic_srv_restart='sudo supervisorctl restart logic_srv'
alias logic_srv_start='sudo supervisorctl start logic_srv'
alias logic_srv_stop='sudo supervisorctl stop logic_srv'

alias celery_restart='sudo supervisorctl restart parkzap-celery'
alias celery_start='sudo supervisorctl start parkzap-celery'
alias celery_stop='sudo supervisorctl stop parkzap-celery'

alias celerybeat_restart='sudo supervisorctl restart parkzap-celerybeat'
alias celerybeat_start='sudo supervisorctl start parkzap-celerybeat'
alias celerybeat_stop='sudo supervisorctl stop parkzap-celerybeat'

alias celerycam_restart='sudo supervisorctl restart parkzap-celerycam'
alias celerycam_start='sudo supervisorctl start parkzap-celerycam'
alias celerycam_stop='sudo supervisorctl stop parkzap-celerycam'

alias pgbouncer_restart='sudo supervisorctl restart conn_pooler'
alias pgbouncer_start='sudo supervisorctl start conn_pooler'
alias pgbouncer_stop='sudo supervisorctl stop conn_pooler'

#supervisord commands for logic_Srv2
alias logic_srv1_restart='sudo supervisorctl restart logic_srv2'
alias logic_srv1_start='sudo supervisorctl start logic_srv2'
alias logic_srv1_stop='sudo supervisorctl stop logic_srv2'


alias flower_restart='sudo supervisorctl restart parkzap-flower'
alias flower_start='sudo supervisorctl start parkzap-flower'
alias flower_stop='sudo supervisorctl stop parkzap-flower'

# super visior commands for logic_srv_project(tollmequick)
alias logic_srv_project_restart='sudo supervisorctl restart logic_srv'
alias logic_srv_project_start='sudo supervisorctl start logic_srv'
alias logic_srv_project_stop='sudo supervisorctl stop logic_srv'

#check celery's status
    #See gunicorn log
    alias logic_srv_log1='tail /opt/tmq_django/logs/gunicorn_supervisor.log'
    alias logic_srv_log2='tail /opt/tmq_django/logs/gunicorn_basic.log'
    # parkzap_nginx log
    alias logic_srv_nginx_access_log='tail /opt/tmq_django/logs/nginx-access.log'
    alias logic_srv_nginx_error_log='tail /opt/tmq_django/logs/nginx-error.log'
    #celery log
    alias celery_stderr='tail /opt/tmq_django/logs/celery_stderr.log'
    alias celery_stdout='tail /opt/tmq_django/logs/celery_stdout.log'
    alias celery_stdout='tail /opt/tmq_django/logs/celery_supervisor.log'
    #nginx log


#supervisior for tollmequick
alias tollmequick_restart='sudo supervisorctl restart logic_srv_project'
alias tollmequick_start='sudo supervisorctl start logic_srv_project'
alias tollmequick_stop='sudo supervisorctl stop logic_srv_project'
#See gunicorn log tollmequick
    alias tollmequick_log1='tail /opt/tmq_django/logs/gunicorn_supervisor.log'
    alias tollmequick_log2='tail /opt/tmq_django/logs/gunicorn_basic.log'
    # tollmequick_nginx log
    alias tollmequick_nginx_access_log='tail /opt/tmq_django/logs/nginx-access.log'
    alias tollmequick_nginx_error_log='tail /opt/tmq_django/logs/nginx-error.log'

#nginx
alias nginx_start='sudo service nginx start'
alias nginx_restart='sudo service nginx restart'
alias nginx_stop='sudo service nginx stop'
alias nginx_status='sudo service nginx status'

#See Logs on SSH server
